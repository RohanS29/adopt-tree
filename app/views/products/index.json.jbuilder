json.array!(@products) do |product|
  json.extract! product, :id, :name, :image_url, :description, :medical_use, :price, :oxygen_production_per_day_max_height_it_can_grow_in_feet
  json.url product_url(product, format: :json)
end
