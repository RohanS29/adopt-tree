class Product < ActiveRecord::Base
  :image_filename
  
  validates :name, :image_url, :description, :medical_use, :price, :oxygen_production_per_day_max_height_it_can_grow_in_feet,  :presence => true
  validates :price, :numericality => {:greater_than_or_equal_to => 0.01}
  validates :name, :uniqueness => true
  end
