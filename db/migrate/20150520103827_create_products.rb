class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :image_url
      t.text :description
      t.text :medical_use
      t.decimal :price
      t.string :oxygen_production_per_day_max_height_it_can_grow_in_feet

      t.timestamps null: false
    end
  end
end
